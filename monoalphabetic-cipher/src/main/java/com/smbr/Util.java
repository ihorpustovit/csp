package com.smbr;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Consumer;

public final class Util {
    public static final void readInputStreamAndPerformAction(InputStream inputStream, Consumer<Character> action) throws IOException {
        for (int i = 0,bt = inputStream.read();
             bt != -1;
             bt = inputStream.read(),i++){
            action.accept((char) bt);
        }
    }
}
