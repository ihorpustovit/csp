package com.smbr;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.function.Predicate;

@Component
public class CesarCryptographer implements FileCryptographer{
    @Value("${cryptographer.cesar.key-step}")
    private int keyStep;

    @Override
    @SneakyThrows
    public ByteArrayResource encrypt(InputStream inputStream) {
        byte[] encryptedBytes = new byte[inputStream.available()];

        for (int i = 0,bt = inputStream.read();
             bt != -1;
             bt = inputStream.read(),i++){

             encryptedBytes[i] = encryptAsciiByte(bt);
        }

        return new ByteArrayResource(encryptedBytes);
    }

    private byte encryptAsciiByte(int bt){
        System.out.println((char)bt);
        int nextByte = bt;

        if (Constants.isUppercaseLetter.or(Constants.isLowercaseLetter).test((char)bt)){
            nextByte = bt + keyStep;
            if (Constants.isLowercaseLetter.test((char)bt)){
                if (nextByte > Constants.lastLowercaseLetterIndex){
                    nextByte = (Constants.firstLowercaseLetterIndex - 1) + (nextByte - Constants.lastLowercaseLetterIndex);
                }
            } else if (Constants.isUppercaseLetter.test((char)bt)){
                if (nextByte > Constants.lastUppercaseLetterIndex){
                    nextByte = (Constants.firstUppercaseLetterIndex - 1) + (nextByte - Constants.lastUppercaseLetterIndex);
                }
            }
        }

        return (byte) nextByte;
    }
}
