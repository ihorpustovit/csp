package com.smbr;

import lombok.SneakyThrows;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class FrequencyAnalyzingDecryptographer implements FileDecryptographer{
    @Override
    @SneakyThrows
    public ByteArrayResource decrypt(MultipartFile multipartFile) {
        InputStream inputStream = multipartFile.getInputStream();
        byte[] decryptedBytes = new byte[inputStream.available()];

        List<Constants.LetterStatistics> givenTextStatistics = performTextStatistics(multipartFile.getInputStream(),Constants.ignoringCharacters);
        Map<Character,Character> decryptingTable = givenTextStatistics.stream()
                .collect(Collectors.toMap(Constants.LetterStatistics::getLetter,this::getMostCommonLetterByEnglishStatistics));


        for (int i = 0,bt = inputStream.read();
             bt != -1;
             bt = inputStream.read(),i++){

            try {

                if (Constants.isLowercaseLetter.test((char)bt) || Constants.isUppercaseLetter.test((char)bt)){
                    decryptedBytes[i] = (byte) decryptingTable.get((char)bt).charValue();
                } else {
                    decryptedBytes[i] = (byte) bt;
                }
            } catch (NullPointerException e){
                System.out.println();
            }
        }

        return new ByteArrayResource(decryptedBytes);
    }

    @SneakyThrows
    private List<Constants.LetterStatistics> performTextStatistics(InputStream inputStream, List<Character> ignoringChars){
        Map<Character,Long> totalOccurrencesMap = new HashMap<>();

        Util.readInputStreamAndPerformAction(inputStream,character -> {
            if (!ignoringChars.contains(character) && Constants.isUppercaseLetter.or(Constants.isLowercaseLetter).test(character)){
                if (totalOccurrencesMap.get(character) != null){
                    totalOccurrencesMap.put(character,totalOccurrencesMap.get(character) + 1);
                } else {
                    totalOccurrencesMap.put(character,1L);
                }
            }
        });

        Long totalFrequency = totalOccurrencesMap.values().stream().mapToLong(f -> f).sum();

        totalOccurrencesMap.forEach((key,value) -> System.out.println(key + " - " + value));
        return totalOccurrencesMap.entrySet().stream().map(entry -> new Constants.LetterStatistics(entry.getKey(),  (((double)entry.getValue()) / ((double)totalFrequency)) * 100.0))
                .collect(Collectors.toList());
    }

    private Character getMostCommonLetterByEnglishStatistics(Constants.LetterStatistics letterStatistics){
        List<Constants.LetterStatistics> letterStatisticsList = new ArrayList<>(Constants.englishLettersStatistics);
        ListIterator<Constants.LetterStatistics> letterStatisticsIterator = letterStatisticsList.listIterator();
        Character mostCommonLetter = null;

        while (letterStatisticsIterator.hasNext()){
            Constants.LetterStatistics basicLetterStatistics = letterStatisticsIterator.next();
            if (Objects.equals(basicLetterStatistics.getFrequency(),letterStatistics.getFrequency())){
                mostCommonLetter = basicLetterStatistics.getLetter();
                break;
            } else if (basicLetterStatistics.getFrequency() > letterStatistics.getFrequency()){
                Constants.LetterStatistics previousLetterStatistics = letterStatisticsIterator.previous();

                if (Objects.isNull(previousLetterStatistics)){
                    mostCommonLetter = basicLetterStatistics.getLetter();
                } else {
                    mostCommonLetter = previousLetterStatistics.getLetter();
                }

                break;
            } else if (Objects.isNull(mostCommonLetter) && !letterStatisticsIterator.hasNext()){
                mostCommonLetter = basicLetterStatistics.getLetter();
            }
        }

        return (char)(Constants.isLowercaseLetter.test(letterStatistics.getLetter()) && Objects.nonNull(mostCommonLetter)? mostCommonLetter + 32 : mostCommonLetter);
    }
}
