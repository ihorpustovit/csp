package com.smbr;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

@Component
public class CesarFileDecryptographer implements FileDecryptographer{
    @Value("${cryptographer.cesar.key-step}")
    private int keyStep;

    @Override
    @SneakyThrows
    public ByteArrayResource decrypt(MultipartFile multipartFile) {
        InputStream inputStream = multipartFile.getInputStream();
        byte[] encryptedBytes = new byte[inputStream.available()];
        for (int i = 0,bt = inputStream.read();
             bt != -1;
             bt = inputStream.read(),i++){
            encryptedBytes[i] = (byte) (Character.isAlphabetic(bt) ? (bt - keyStep) : bt);
        }

        return new ByteArrayResource(encryptedBytes);
    }
}
