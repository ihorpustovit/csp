package com.smbr;

import java.math.BigInteger;
import java.util.function.Predicate;

public final class RsaUtil {
    public static boolean isPrime(long test){
        boolean isPrime = true;
        for (int i = 2; i <= Math.sqrt(test); i++){
            if (test % i == 0){
                isPrime = false;
                break;
            }
        }

        return isPrime;
    }

    public static boolean isMutuallyPrime(long a, long b){
        return common_divisors(a,b) == 1;
    }

    static long find_gcd(long val_1,long val_2){
        if (val_1 == 0)
            return val_2;
        return find_gcd(val_2%val_1,val_1);
    }
    static int common_divisors(long val_1,long val_2){
        long no = find_gcd(val_1, val_2);
        int result = 0;
        for (int i=1; i<=Math.sqrt(no); i++){
            if (no%i==0){
                if (no/i == i)
                    result += 1;
                else
                    result += 2;
            }
        }
        return result;
    }

    public static long generate(int decades){
        return (long)Math.abs((Math.random() * decadesToNumber(decades)));
    }

    private static long decadesToNumber(int decades){
        StringBuilder a = new StringBuilder("1");

        while (decades >= 1){
            decades--;
            a.append("0");
        }

        return Long.parseLong(a.toString());
    }

    public static long generatePrime(int decades, Predicate<Long> predicate){
        long primeGeneratedNumber = 0;
        do{
            primeGeneratedNumber = generate(decades);
        } while (!isPrime(primeGeneratedNumber) || primeGeneratedNumber <= 1 || predicate.negate().test(primeGeneratedNumber));
        return primeGeneratedNumber;
    }
}
