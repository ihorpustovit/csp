package com.smbr;

public class RsaCryptographer{
    private static final int q;
    private static final int p;
    private static final int n;
    private static final int d;
    private static final int e;

    static {
        q = 13;
        p = 2;
        n = p * q;
        d = 5;
        e = 17;
    }

    public RsaCryptographer(){}

    public static String encrypt(String text){
        StringBuilder sb = new StringBuilder();
        for (byte a : text.getBytes()){
            if (Constants.isUppercaseLetter.test((char) a)){
                sb.append((char) ((Math.pow(a - 65,e) % n) + 65));
            } else if (Constants.isLowercaseLetter.test((char)a)){
                sb.append((char) ((Math.pow(a - 97,e) % n) + 97));
            }
        }
        return sb.toString();
    }

    public static String decrypt(String text){
        StringBuilder sb = new StringBuilder();
        for (byte a : text.getBytes()){
            if (Constants.isUppercaseLetter.test((char) a)){
                sb.append((char) ((Math.pow(a - 65,d) % n) + 65));
            } else if (Constants.isLowercaseLetter.test((char)a)){
                sb.append((char) ((Math.pow(a - 97,d) % n) + 97));
            }
        }
        return sb.toString();
    }


    public static void main(String[] args) {
        String dehaka = "DEHAKA";
        String enc = RsaCryptographer.encrypt(dehaka);
        System.out.println(enc);
        System.out.println(RsaCryptographer.decrypt(enc));
    }
}
