package com.smbr;

public interface Decryptographer<S,R> {
    R decrypt(S s);
}
