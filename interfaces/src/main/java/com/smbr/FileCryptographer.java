package com.smbr;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

public interface FileCryptographer extends Cryptographer<InputStream, ByteArrayResource>{}
