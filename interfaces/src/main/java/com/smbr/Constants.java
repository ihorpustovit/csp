package com.smbr;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.Assert;

import java.util.*;
import java.util.function.Predicate;

@SuppressWarnings("unchecked")
public interface Constants {
    int firstUppercaseLetterIndex = 65;
    int lastUppercaseLetterIndex = 90;
    int firstLowercaseLetterIndex = 97;
    int lastLowercaseLetterIndex = 122;

    Predicate<Character> isUppercaseLetter = character -> firstUppercaseLetterIndex <= character && character <= lastUppercaseLetterIndex;
    Predicate<Character> isLowercaseLetter = character -> firstLowercaseLetterIndex <= character && character <= lastLowercaseLetterIndex;

    List<Character> ignoringCharacters = new ArrayList<>(Arrays.asList((char) 32,'\n'));

    Set<LetterStatistics> englishLettersStatistics = new TreeSet<>(
            Arrays.asList(
                    new LetterStatistics('A',8.34),
                    new LetterStatistics('B',1.54),
                    new LetterStatistics('C',2.73),
                    new LetterStatistics('D',4.14),
                    new LetterStatistics('E',12.60),
                    new LetterStatistics('F',2.03),
                    new LetterStatistics('G',1.92),
                    new LetterStatistics('H',6.11),
                    new LetterStatistics('I',6.71),
                    new LetterStatistics('J',0.23),
                    new LetterStatistics('K',0.87),
                    new LetterStatistics('L',4.24),
                    new LetterStatistics('M',2.53),
                    new LetterStatistics('N',6.80),
                    new LetterStatistics('O',7.70),
                    new LetterStatistics('P',1.66),
                    new LetterStatistics('Q',0.09),
                    new LetterStatistics('R',5.68),
                    new LetterStatistics('S',6.11),
                    new LetterStatistics('T',9.37),
                    new LetterStatistics('U',2.85),
                    new LetterStatistics('V',1.06),
                    new LetterStatistics('W',2.34),
                    new LetterStatistics('X',0.20),
                    new LetterStatistics('Y',2.04),
                    new LetterStatistics('Z',0.06)
                    )
    );

    static int indexOfEnglishAlphabetLetter(Character character){
        if (Character.isAlphabetic(character)){
            character = Character.isUpperCase(character) ? character : Character.toUpperCase(character);
            return ((byte)character.charValue()) - Constants.firstUppercaseLetterIndex + 1;
        }
        throw new IllegalArgumentException();
    }

    static char letterByIndex(int index,boolean isUppercase){
        char resultValue = (char) (index + Constants.firstUppercaseLetterIndex - 1);
        Assert.isTrue(Character.isAlphabetic(resultValue), () -> "No letter for such an index");
        return isUppercase ? Character.toUpperCase(resultValue) : Character.toLowerCase(resultValue);
    }

    @AllArgsConstructor
    @Getter
    @Setter
    class LetterStatistics implements Comparable<LetterStatistics>{
        private Character letter;
        private Double frequency;

        @Override
        public int hashCode() {
            return Objects.hash(letter,frequency);
        }

        @Override
        public int compareTo(LetterStatistics o) {
            if (this.frequency > o.frequency){
                return 1;
            } else if (this.frequency < o.frequency) {
                return -1;
            }
            return 0;
        }

        @Override
        public boolean equals(Object obj) {
            if (Objects.isNull(obj)){
                return false;
            } else if (Objects.equals(obj.getClass(),this.getClass())){
                LetterStatistics o = (LetterStatistics)obj;
                return Objects.equals(this.frequency,o.frequency) && Objects.equals(this.letter,o.letter);
            }

            return false;
        }
    }
}
