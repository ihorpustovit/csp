package com.smbr;

public interface Cryptographer<S,R> {
    R encrypt(S s);
}
