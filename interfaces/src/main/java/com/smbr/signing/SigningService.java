package com.smbr.signing;

public interface SigningService<M extends Message<?>,SM extends SignedMessage> {
    int p = 23;
    int g = 5;

    SM sign(M message);

    boolean isSignConsistent(SM signedMessage);
}
