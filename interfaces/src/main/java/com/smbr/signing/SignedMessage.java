package com.smbr.signing;

public interface SignedMessage {
    int getHash();
}
