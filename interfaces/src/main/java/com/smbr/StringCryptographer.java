package com.smbr;

import lombok.SneakyThrows;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public interface StringCryptographer extends ByteCryptographer {
    @Override
    @SneakyThrows
    default ByteArrayOutputStream encrypt(ByteArrayInputStream inputStream) {
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes);
        byte[] resultBytes = encrypt(new String(bytes)).getBytes();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(resultBytes.length);
        byteArrayOutputStream.write(resultBytes);
        return byteArrayOutputStream;
    }

    String encrypt(String input);
}
