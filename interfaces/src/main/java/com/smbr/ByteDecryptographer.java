package com.smbr;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public interface ByteDecryptographer extends Decryptographer<ByteArrayInputStream, ByteArrayOutputStream>{
}
