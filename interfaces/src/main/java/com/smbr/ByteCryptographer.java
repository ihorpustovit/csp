package com.smbr;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public interface ByteCryptographer extends Cryptographer<ByteArrayInputStream, ByteArrayOutputStream>{
}
