package com.smbr;

import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

public interface FileDecryptographer extends Decryptographer<MultipartFile, ByteArrayResource>{
}
