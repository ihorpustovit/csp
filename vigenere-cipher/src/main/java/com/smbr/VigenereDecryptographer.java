package com.smbr;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static com.smbr.Constants.indexOfEnglishAlphabetLetter;

@Component
public class VigenereDecryptographer implements ByteDecryptographer{
    private final String key;

    public VigenereDecryptographer(@Value("${cryptographer.vigenere.key}") String key){
        this.key = key;
    }

    @Override
    @SneakyThrows
    public ByteArrayOutputStream decrypt(ByteArrayInputStream inputStream) {
        byte[] decryptedBytes = new byte[inputStream.available()];
        for (int i = 1, bt = inputStream.read();
             bt != -1;
             bt = inputStream.read(),i++){

            if (Character.isAlphabetic(bt)){
                int indexOfKeyLetter = (i % key.length()) == 0 ? key.length() : (i % key.length());
                int decodedLetterIndex = (Constants.indexOfEnglishAlphabetLetter((char)bt) - Constants.indexOfEnglishAlphabetLetter(key.charAt(indexOfKeyLetter - 1)) + 26) % 26;
                decryptedBytes[i - 1] = (byte)Constants.letterByIndex(decodedLetterIndex == 0? 26 : decodedLetterIndex,Character.isUpperCase(bt));
            } else {
                decryptedBytes[i - 1] = (byte) bt;
            }

        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(decryptedBytes);
        return byteArrayOutputStream;
    }
}
