package com.smbr;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;

@Component
public class VigenereCryptographer implements ByteCryptographer{
    private final java.lang.String key;

    public VigenereCryptographer(@Value("${cryptographer.vigenere.key}") String key){
        this.key = key;
    }

    @Override
    @SneakyThrows
    public ByteArrayOutputStream encrypt(ByteArrayInputStream inputStream) {
        byte[] encryptedBytes = new byte[inputStream.available()];
        int insertCount = 0;

        for (int i = 1, bt = inputStream.read();
             bt != -1;
             bt = inputStream.read(),i++){

            if (Constants.isUppercaseLetter.or(Constants.isLowercaseLetter).test((char)bt)){
                int indexOfKeyLetter = (i % key.length()) == 0 ? key.length() : (i % key.length());
                int encodedLetterIndex = (Constants.indexOfEnglishAlphabetLetter(key.charAt(indexOfKeyLetter - 1)) + Constants.indexOfEnglishAlphabetLetter((char)bt)) % 26;
                encryptedBytes[insertCount++] = (byte)(Constants.letterByIndex(encodedLetterIndex == 0? 26 : encodedLetterIndex,Character.isUpperCase(bt)));
            }

//            else {
//                encryptedBytes[i - 1] = (byte) bt;
//            }
        }

        byte[] encryptedBytesCopy = new byte[insertCount];

        for (int i = 0; i < insertCount; i++){
            encryptedBytesCopy[i] = encryptedBytes[i];
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(encryptedBytesCopy);
        return byteArrayOutputStream;
    }

}
