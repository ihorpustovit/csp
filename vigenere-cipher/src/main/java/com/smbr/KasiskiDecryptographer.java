package com.smbr;

import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

public class KasiskiDecryptographer implements ByteDecryptographer{
    private int repeatableSubstringSize;

    @Override
    public ByteArrayOutputStream decrypt(ByteArrayInputStream inputStream) {
        String targetString = covertToString(inputStream);
        Set<String> repeatableStrings = repeatableSubstrings(targetString);
        Map<String,List<Long>> repeatableStringsIndexes = repeatableStrings.stream()
                .collect(Collectors.toMap(s -> s,s -> occurrencesIndexes(targetString,s)));
        List<Long> indexesDifferences = repeatableStringsIndexes.entrySet().stream()
                .map(entry -> {
                    if (entry.getValue().size() == 1){
                        return entry.getValue().get(0);
                    } else {
                        return Math.abs(entry.getValue().get(0) - entry.getValue().get(entry.getValue().size() - 1));
                    }
                })
                .collect(Collectors.toList());
        return null;
    }

    private static String covertToString(ByteArrayInputStream inputStream){
        byte[] bytes = new byte[inputStream.available()];
        inputStream.read(bytes,0,inputStream.available());
        return new String(bytes);
    }

    private static Set<String> repeatableSubstrings(final String targetString){
        int startIndex = 0;
        int endIndex = 3;
        Set<String> repeatableSubstrings = new HashSet<>();

        while (endIndex <= targetString.length()){
            String substr = targetString.substring(startIndex,endIndex);
            if (StringUtils.isAlpha(substr) &&
                    StringUtils.countMatches(targetString,substr) > 1){
                repeatableSubstrings.add(targetString.substring(startIndex,endIndex));
            }
            startIndex++;
            endIndex++;
        }
        return repeatableSubstrings;
    }

    private static List<Long> occurrencesIndexes(String str,String sub){
        int prevIndex = 0;
        List<Long> occurrencesIndexes = new ArrayList<>();

        while (str.contains(sub)){
            occurrencesIndexes.add((long)(str.indexOf(sub) + prevIndex));
            prevIndex = str.indexOf(sub) + sub.length();
            str = str.substring(prevIndex);
        }
        return occurrencesIndexes;
    }

    private static Long commonGCD(List<Long> values){
        while (values.size() > 1){
            long gcd = BigInteger.valueOf(values.get(0)).gcd(BigInteger.valueOf(values.get(1))).intValue();
            values.set(1,gcd);
            values = values.subList(1,values.size());
        }

        return values.get(0);
    }

    private static List<Integer> factorsOf(Long n){
        List<Integer> factors = new ArrayList<>();
        for (int loopCounter = 1; loopCounter <= n; loopCounter++) {
            // check if remainder of division is 0
            if (n % loopCounter == 0) {
                factors.add(loopCounter);
            }
        }
        return factors;
    }

    private static List<String> splitAtLength(String targetString, int atLength){
        List<String> subs = new ArrayList<>();

        while (targetString.length() >= atLength){
            subs.add(targetString.substring(0,atLength));
            targetString = targetString.substring(atLength);
        }

        return subs;
    }

    @SneakyThrows
    public static void main(String[] args) {
        String targetString = "LsidwwwbsgjdwxsfteywwwvmlavhxslzerdnhxfglxbxqgedidjwjgxuqmnqvymwdqludodjhNwslxdkuknrjkkjdhgdktylwwwujvivknhcvBkpnunqlxxzdjbwXxtahsdhyktjkjdhgdwwailudodjhnsgtjimfqskxahgqjanqvxahcrhtnhqutxisanosjugXxtahsmmlgvkjdhgdktbbahhygIgsdvjlzukjwwbxwmdzwbsdwfddtKtdxjghsajhgadzxeBkpdbyktkuwhpkegxpjusriugtxvzNlabegtvyvfwtjajqgedwdjajrzyeiujefzwgcykxkzwrlfxewkjzQeqvjepskixlwhxrcuuhkxdtkjqsbmmrjyxkhleywwwcwhbwcujuajDcvltXshwutujrbvuwnfplyhsEghhoxxsXmtxhandhpjitosYxdlqrdvfyynftfjulrlkkjxftedgafpdbxiIjkxVighbjvxhhrCsjnwhqutxijajejyfjypdytutkjLimtxpfbhwjjutisutduegxwgasmtuadyktqvydxlbdadepnqvqgdcactqZukjxktfdhotilxxggsmbsjYgwwwutrzymvpatQgqhrckjknfighlvlsbetzlxxnuhhxdlzeejoymmrjlvmhladzliTkwtjmmdijajbshxqdltgotjhpdnudcvmmhniejhejawrjyxyktibcbgdmmvlxtyizurqtwtkrgtblhhlyhsYurcvukjgvuxuonmmhchahgjajpvlxswjjulrujajymdzqhQgipxjxwhgcxzdjabliztfddekjghughlaYxxruuxihsygppcyglbqynuhlwwdladzBqWwdladzQjerxwDfuLibhtntvenntjanvNhzepjgeordhlukultuuyrlxxjggmgmflpftfvzwtyktepmhizukwwwwwdladzigawayhcwtykteGxijajbsdlbhgwtUjyzmwtfBkniatxavtsbgdxetvknjwlugjgtofwsjBqwwdladzzpkgtwsinfimhxruqmdiBylsifeasmzuteydtetfdfimwlrlekgxyulylcytstduimdclGxiibsftjajvjepskixlwhxqdltgotjhxcvukxwpftnwYrdswtsrizukggsmbsjYiutommhadlngteywwwutduegxwgasmtuihwwsjyktwktzczshshzosixjxlvqhpjbrIzurdaoqrxcwuwwdxtahlxbsjhuquopadxiEoIupoyglCmcujuLmhodgaxiaaaxwwaiIzuludodjhiutkfhsvtmmlhjbrhmtxigtiyxkurhleqdnqlngtcrggsmbsjheyedsvtqhlhbhwdjizwwjajuxhhrizunqhatxrgjajdmjlngttsgvuotwtcrxhaxbsviwqwwdwxtjgsfadzylyrgqfuxlxfjwxutsgyhtrppjYkplbxlzodijajpyutikyqXwtahmfbkplfnjwlafytrxjqsffjcavbhhclvfutwhfvsiflclukXxtiqwuggxkxxfuiwdxiqqmmhxqbqxgwhkbqWwdladzQjerxwDfufqscrGgsmbsjFkfghgJptYhhbqnuvfuojumdwjuhlqgipfommlcyudizufxhanulpftnwaiylgwihrhxekfwabwwhcjhetqebdnktsgxekjytjxcsasygnqvjanqvkmtizufHgmmhcYhkdkufqdlxxwejeyjvhaegpftqhpjdxiiginodltnuedqgjvYmdkwyqrlftoxljejdnukdadifuikhkizubrgdtdcvbyxkmwxtjafwyuhluphxrkpkujhclxwbmixkxajhptTypwefqrwBfpfwnviadzzlhzVmlcsywrbQkncdfqLuegjvwjlodkjnqlxxqxyxmhmsancgmejgvwbxksbnfeawNqlxxfdmhljdxmmlhbbkhAafytxtipwkjdictsbwdvtxclukxlajadyhxfweqgdeweiqhoxhkpnughtfvtqrwhgjgoymmbsjmjuheyfdfixvxtfsxXxtahdyojgszwhplihpdtrrcyzwrlfxekNwslxvtwdyktebswxeqmjonvqrhwtywsdwPftykplafvcjrxrzbrsgglxibqhulcaegrujajpMajqtnukLeumrcwhkizufzwgljhbwtyreufwsbefawqkkyzmwtvNijyxiizujaewhbrhclhkhzepnqvxbrbqWwdladzQjerxwDfuzwasaLzqojpdmtdvcuiyAptxavmwbleklcvhzwihxxmmlhmtxpfxwvdfhkijkxxcvukxwpftbsjRnyoxhjytjbylsikthwhzuzdmbwdaoqrxhsoIzqmlhqmdiYktfBzdmbwqtnukwpdayrlxtyewhltqsrhzwtetfdfimwlrlekxghuuxeuofoxekjviktukjtwvYbrjdtguxfwrbhwbygdodyrzylotnueXmhzosjtqnlemlbqutxirkngvwfqswhqiqgiegbbylrkfqsdxhniaulPftyktwktzczsoenqgtulutsjededutxhsjhkpnurhiinhksljqharejbsd";

        Set<String> repeatableStrings = repeatableSubstrings(targetString);

        Map<String,List<Long>> repeatableStringsIndexes = repeatableStrings.stream()
                .collect(Collectors.toMap(s -> s,s -> occurrencesIndexes(targetString,s)))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().size() == 2)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        List<Long> indexesDifferences = repeatableStringsIndexes.entrySet().stream()
                .map(entry -> Math.abs(entry.getValue().get(0) - entry.getValue().get(entry.getValue().size() - 1)))
                .distinct()
                .collect(Collectors.toList());
        List<Long> selectedIndexes = indexesDifferences.stream()
                .collect(Collectors.toMap(l -> l, KasiskiDecryptographer::factorsOf))
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue().size() >= 7 && entry.getKey() < 100)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        long commonGCD = commonGCD(new ArrayList<>(selectedIndexes));

        System.out.println(commonGCD);
    }
}
