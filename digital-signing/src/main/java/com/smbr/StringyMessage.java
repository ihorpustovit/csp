package com.smbr;

import com.smbr.signing.Message;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Objects;

@RequiredArgsConstructor
@Getter
public class StringyMessage implements Message<String> {
    private final String stringyMessage;
    private final int defaultHash = 12;
    @Getter(AccessLevel.PRIVATE)
    private final boolean useDefaultHash;

    @Override
    public String getMessage() {
        return stringyMessage;
    }

    public int getHash() {
        return useDefaultHash ? defaultHash : stringyMessage.length();
    }
}
