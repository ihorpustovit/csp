package com.smbr.elgamal;

import com.smbr.StringyMessage;
import com.smbr.signing.SignedMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@AllArgsConstructor
public class ElGamalSignedMessage implements SignedMessage {
    @Setter
    private StringyMessage stringyMessage;
    private final int a;
    private final int b;

    @Override
    public int getHash() {
        return 3;
    }
}
