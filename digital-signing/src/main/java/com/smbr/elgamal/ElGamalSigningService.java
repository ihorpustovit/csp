package com.smbr.elgamal;

import com.smbr.StringyMessage;
import com.smbr.signing.SigningService;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.function.Predicate;

public class ElGamalSigningService implements SigningService<StringyMessage, ElGamalSignedMessage> {
    private final int x = 5;
    private final int k = 5;
    private final int y = (int) (Math.pow(g, x) % p);
    private final int a = (int) (Math.pow(g, k) % p);
    private final int b;

    {
        Predicate<Integer> isSuitable = i -> (((x * a) + (k * i)) % (p - 1)) == 3;
        int trialB = 0;
        while (isSuitable.negate().test(trialB)) {
            trialB++;
        }
        this.b = trialB;
    }

    @Override
    public ElGamalSignedMessage sign(StringyMessage message) {
        return new ElGamalSignedMessage(message, a, b);
    }

    @Override
    public boolean isSignConsistent(ElGamalSignedMessage signedMessage) {
        BigInteger yObject = new BigInteger(String.valueOf(y));
        BigInteger aObject = new BigInteger(String.valueOf(signedMessage.getA()));
        BigInteger pObject = new BigInteger(String.valueOf(p));

        long rightPart = yObject.pow(signedMessage.getA()).multiply(aObject.pow(signedMessage.getB())).mod(pObject).intValue();
        long leftPart = new BigDecimal(g).pow(signedMessage.getHash()).intValue() % p;

        return rightPart == leftPart;
    }

    public static void main(String[] args) {
        ElGamalSigningService elGamalSigningService = new ElGamalSigningService();
        String plainMessage = "aadg";
        ElGamalSignedMessage elGamalSignedMessage = elGamalSigningService.sign(new StringyMessage(plainMessage, true));
        System.out.println(elGamalSigningService.isSignConsistent(elGamalSignedMessage));
    }
}
