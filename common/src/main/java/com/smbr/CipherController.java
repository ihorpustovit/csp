package com.smbr;

import com.smbr.FileCryptographer;
import com.smbr.FileDecryptographer;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequiredArgsConstructor
public class CipherController {
    private final FileCryptographer cesarCryptographer;
    private final FileDecryptographer cesarFileDecryptographer;
    private final FileDecryptographer frequencyAnalyzingDecryptographer;


    @GetMapping("/encrypt")
    @SneakyThrows
    public ResponseEntity<Resource> encrypt(@RequestParam("encryption_file") MultipartFile encryptionFile){
        ByteArrayResource byteArrayResource = cesarCryptographer.encrypt(encryptionFile.getInputStream());
        return ResponseEntity.status(HttpStatus.OK)
                .header("Content-Disposition","attachment; filename="+encryptionFile.getResource().getFilename()+"_encrypted.txt")
                .body(byteArrayResource);
    }

    @GetMapping("/decrypt")
    @SneakyThrows
    public ResponseEntity<Resource> decrypt(@RequestParam("encryption_file") MultipartFile encryptionFile){
        ByteArrayResource byteArrayResource = cesarFileDecryptographer.decrypt(encryptionFile);
        return ResponseEntity.status(HttpStatus.OK)
                .header("Content-Disposition","attachment; filename="+encryptionFile.getResource().getFilename()+".txt")
                .body(byteArrayResource);
    }
}
