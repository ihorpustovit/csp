package com.smbr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import java.io.ByteArrayInputStream;

@SpringBootApplication
public class App implements CommandLineRunner {
    @Autowired
    private RsaCryptographer byteCryptographer;

    @Autowired
    private RsaDecryptographer byteDecryptographer;

    @Override
    public void run(String... args) throws Exception {
        String targetString = "I ask the indulgence of the children who may read this book for dedicating\n" +
                "it to a grown−up. I have a serious reason: he is the best friend I have in\n" +
                "the world. I have another reason: this grown−up understands everything,\n" +
                "even books about children. I have a third reason: he lives in France where\n" +
                "he is hungry and cold. He needs cheering up. If all these reasons are not\n" +
                "enough, I will dedicate the book to the child from whom this grown−up\n" +
                "grew. All grown−ups were once children−− although few of them\n" +
                "remember it. And so I correct my dedication:\n" +
                "\n" +
                "Once when I was six years old I saw a magnificent picture in a book, called True\n" +
                "Stories from Nature, about the primeval forest. It was a picture of a boa constrictor\n" +
                "in the act of swallowing an animal. Here is a copy of the drawing.\n" +
                "In the book it said: \"Boa constrictors swallow their prey whole, without chewing\n" +
                "it. After that they are not able to move, and they sleep through the six months that\n" +
                "they need for digestion.\"\n" +
                "I pondered deeply, then, over the adventures of the jungle. And after some work\n" +
                "with a colored pencil I succeeded in making my first drawing. My Drawing\n" +
                "Number One. It looked like this:\n" +
                "\n" +
                "\n" +
                "I showed my masterpiece to the grown−ups, and asked them whether the drawing\n" +
                "frightened them.\n" +
                "But they answered: \"Frighten? Why should any one be frightened by a hat?\"\n" +
                "My drawing was not a picture of a hat. It was a picture of a boa constrictor\n" +
                "digesting an elephant. But since the grown−ups were not able to understand it, I\n" +
                "made another drawing: I drew the inside of the boa constrictor, so that the\n" +
                "grown−ups could see it clearly. They always need to have things explained. My\n" +
                "Drawing Number Two looked like this:\n" +
                "\n" +
                "The grown−ups' response, this time, was to advise me to lay aside my drawings of\n" +
                "boa constrictors, whether from the inside or the outside, and devote myself instead\n" +
                "to geography, history, arithmetic and grammar. That is why, at the age of six, I\n" +
                "gave up what might have been a magnificent career as a painter. I had been\n" +
                "disheartened by the failure of my Drawing Number One and my Drawing Number\n" +
                "Two. Grown−ups never understand anything by themselves, and it is tiresome for\n" +
                "children to be always and forever explaining things to them.\n" +
                "So then I chose another profession, and learned to pilot airplanes. I have flown a\n" +
                "little over all parts of the world; and it is true that geography has been very useful\n" +
                "to me. At a glance I can distinguish China from Arizona. If one gets lost in the\n" +
                "night, such knowledge is valuable.\n" +
                "In the course of this life I have had a great many encounters with a great many\n" +
                "people who have been concerned with matters of consequence. I have lived a great\n" +
                "\n" +
                "deal among grown−ups. I have seen them intimately, close at hand. And that\n" +
                "hasn't much improved my opinion of them.\n" +
                "Whenever I met one of them who seemed to me at all clear−sighted, I tried the\n" +
                "experiment of showing him my Drawing Number One, which I have always kept.\n" +
                "I would try to find out, so, if this was a person of true understanding. But,\n" +
                "whoever it was, he, or she, would always say:\n" +
                "\"That is a hat.\" Then I would never talk to that person about boa constrictors, or\n" +
                "primeval forests, or stars. I would bring myself down to his level. I would talk to\n" +
                "him about bridge, and golf, and politics, and neckties. And the grown−up would\n" +
                "be greatly pleased to have met such a sensible man";
//        String encryptedString = new String(byteCryptographer.encrypt(new ByteArrayInputStream("2345".getBytes())));
//        System.out.println(byteDecryptographer.decrypt(new ByteArrayInputStream(encryptedString.getBytes())));
    }

    public static void main(String[] args) {
        SpringApplication.run(App.class,args);
    }
}
